
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
       m = (x + y) / 2.0;
       m = berechneMittelwert(x,y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   
   printMittelwert(x,y,m);
   
   
   }
   public static void printMittelwert (double zahl1, double zahl2, double erg) {
     System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, erg);
   }
   
   public static double berechneMittelwert(double zahl1, double zahl2) {
   double erg = (zahl1 + zahl2) / 2.0 ;
   return erg;
   }

}